use std::fmt::{Display, Formatter};

use rand::prelude::*;

use crate::{algorithm::Algorithm, grid::Grid};

pub struct Sidewinder;

impl Algorithm for Sidewinder {
    fn on(&self, grid: &mut Grid) {
        let mut rng = thread_rng();
        for row in grid.rows() {
            let mut run = vec![];
            for cell in row {
                run.push(cell);
                let early_close = cell.north().is_some() && rng.gen_bool(0.5);
                match cell.east() {
                    Some(east) if !early_close => cell.link(&east),
                    _ => {
                        if let Some(member) = run.choose(&mut rng) {
                            if let Some(north) = member.north() {
                                north.link(member)
                            }
                            run.clear()
                        }
                    }
                }
            }
        }
    }
}

impl Display for Sidewinder {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "sidewinder")
    }
}
