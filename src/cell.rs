use std::{
    collections::HashSet,
    fmt,
    hash::{Hash, Hasher},
    sync::{Arc, RwLock},
};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Position {
    pub row: usize,
    pub column: usize,
}

impl From<(usize, usize)> for Position {
    fn from((row, column): (usize, usize)) -> Self {
        Self { row, column }
    }
}

#[derive(Clone, Debug)]
pub struct Cell(Arc<RwLock<CellInner>>);

impl Cell {
    pub fn position(&self) -> Position {
        self.0.read().unwrap().position
    }

    pub fn set_neighbours(
        &mut self,
        north: Option<Self>,
        south: Option<Self>,
        east: Option<Self>,
        west: Option<Self>,
    ) {
        let mut cell = self.0.write().unwrap();
        cell.north = north;
        cell.south = south;
        cell.east = east;
        cell.west = west;
    }

    pub fn north(&self) -> Option<Self> {
        self.0.read().unwrap().north.clone()
    }

    pub fn south(&self) -> Option<Self> {
        self.0.read().unwrap().south.clone()
    }

    pub fn east(&self) -> Option<Self> {
        self.0.read().unwrap().east.clone()
    }

    #[allow(unused)]
    pub fn west(&self) -> Option<Self> {
        self.0.read().unwrap().west.clone()
    }

    #[allow(unused)]
    pub fn neighbours(&self) -> Vec<Self> {
        let cell = self.0.read().unwrap();
        vec![&cell.north, &cell.south, &cell.east, &cell.west]
            .into_iter()
            .flatten()
            .cloned()
            .collect()
    }

    pub fn link(&self, cell: &Self) {
        self.0.write().unwrap().links.insert(cell.clone());
        cell.0.write().unwrap().links.insert(self.clone());
    }

    #[allow(unused)]
    pub fn unlink(&self, cell: &Self) {
        self.0.write().unwrap().links.remove(cell);
        cell.0.write().unwrap().links.remove(self);
    }

    pub fn is_linked(&self, cell: Option<Self>) -> bool {
        if let Some(cell) = cell {
            self.0.read().unwrap().links.contains(&cell)
        } else {
            false
        }
    }
}

impl Eq for Cell {}

impl<P> From<P> for Cell
where
    P: Into<Position>,
{
    fn from(value: P) -> Self {
        let inner = CellInner {
            position: value.into(),
            north: None,
            south: None,
            east: None,
            west: None,
            links: HashSet::new(),
        };
        Self(Arc::new(RwLock::new(inner)))
    }
}

impl Hash for Cell {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.read().unwrap().hash(state)
    }
}

impl PartialEq for Cell {
    fn eq(&self, other: &Self) -> bool {
        *self.0.read().unwrap() == *other.0.read().unwrap()
    }
}

struct CellInner {
    position: Position,
    north: Option<Cell>,
    south: Option<Cell>,
    east: Option<Cell>,
    west: Option<Cell>,
    links: HashSet<Cell>,
}

impl fmt::Debug for CellInner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Inner")
            .field("position", &self.position)
            .field(
                "links",
                &Vec::from_iter(self.links.iter().map(|cell| cell.position())),
            )
            .finish()
    }
}

impl Eq for CellInner {}

impl Hash for CellInner {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.position.hash(state);
    }
}

impl PartialEq for CellInner {
    fn eq(&self, other: &Self) -> bool {
        self.position == other.position
    }
}
