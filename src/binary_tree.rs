use std::fmt::{Display, Formatter};

use rand::prelude::*;

use crate::{algorithm::Algorithm, grid::Grid};

pub struct BinaryTree;

impl Algorithm for BinaryTree {
    fn on(&self, grid: &mut Grid) {
        let mut rng = thread_rng();
        for cell in grid.cells() {
            if let Some(neighbour) = [cell.north(), cell.east()]
                .iter()
                .flatten()
                .choose(&mut rng)
            {
                cell.link(neighbour)
            }
        }
    }
}

impl Display for BinaryTree {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "binarytree")
    }
}
