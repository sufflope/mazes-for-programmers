#![warn(clippy::explicit_iter_loop)]
#![warn(clippy::flat_map_option)]
#![warn(clippy::if_then_some_else_none)]
#![warn(clippy::use_self)]

use std::fs;

use uuid::Uuid;

use crate::{algorithm::Algorithm, binary_tree::BinaryTree, grid::Grid, sidewinder::Sidewinder};

mod algorithm;
mod binary_tree;
mod cell;
mod grid;
mod sidewinder;

fn main() -> anyhow::Result<()> {
    let algorithms: [Box<dyn Algorithm>; 2] = [Box::new(BinaryTree), Box::new(Sidewinder)];
    for algorithm in algorithms {
        let uuid = Uuid::new_v4();

        let mut grid = Grid::new(4, 4);
        algorithm.on(&mut grid);

        println!("Generated grid {uuid} with algorithm {algorithm}:");
        println!("{grid}");

        let path = |extension| format!("{uuid}.{algorithm}.{extension}");
        fs::write(path("txt"), grid.to_string())?;
        grid.to_png(None).save(path("png"))?
    }
    Ok(())
}
