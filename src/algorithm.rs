use std::fmt::Display;

use crate::grid::Grid;

pub trait Algorithm: Display {
    fn on(&self, grid: &mut Grid);
}
