use std::fmt;

use image::{GrayImage, Luma};

use crate::cell::{Cell, Position};

#[derive(Debug)]
pub struct Grid {
    cells: Vec<Vec<Cell>>,
}

impl Grid {
    pub fn new(rows: usize, columns: usize) -> Self {
        let mut cells = vec![Vec::with_capacity(columns); rows];
        for (r, row) in cells.iter_mut().enumerate() {
            for c in 0..columns {
                row.push((r, c).into())
            }
        }
        let grid = Self { cells };
        for row in 0..rows {
            for column in 0..columns {
                grid.cell(row, column).unwrap().set_neighbours(
                    if row > 0 {
                        grid.cell(row - 1, column)
                    } else {
                        None
                    },
                    grid.cell(row + 1, column),
                    grid.cell(row, column + 1),
                    if column > 0 {
                        grid.cell(row, column - 1)
                    } else {
                        None
                    },
                )
            }
        }
        grid
    }

    pub fn cells(&self) -> impl Iterator<Item = Cell> + '_ {
        match self.width() {
            0 => Cells::Empty,
            columns => Cells::NonEmpty {
                columns,
                i: 0,
                grid: self,
            },
        }
    }

    pub fn rows(&self) -> impl Iterator<Item = impl Iterator<Item = &Cell>> {
        self.cells.iter().map(|row| row.iter())
    }

    fn width(&self) -> usize {
        match self.cells.first() {
            None => 0,
            Some(row) => row.len(),
        }
    }

    fn cell(&self, row: usize, column: usize) -> Option<Cell> {
        self.cells.get(row).and_then(|row| row.get(column).cloned())
    }

    pub fn to_png(&self, cell_size: Option<usize>) -> GrayImage {
        let background: Luma<u8> = Luma::from([u8::MIN]);
        let wall: Luma<u8> = Luma::from([u8::MAX]);

        let cell_size = cell_size.unwrap_or(10);
        let mut image = GrayImage::from_pixel(
            1 + (self.width() * cell_size) as u32,
            1 + (self.rows().count() * cell_size) as u32,
            background,
        );

        for cell in self.cells() {
            let Position { row, column } = cell.position();
            let x1 = (column * cell_size) as u32;
            let y1 = (row * cell_size) as u32;
            let x2 = ((column + 1) * cell_size) as u32;
            let y2 = ((row + 1) * cell_size) as u32;

            if cell.north().is_none() {
                for x in x1..x2 {
                    image.put_pixel(x, y1, wall);
                }
            }

            if cell.west().is_none() {
                for y in y1..y2 {
                    image.put_pixel(x1, y, wall);
                }
            }

            if !cell.is_linked(cell.east()) {
                for y in y1..y2 {
                    image.put_pixel(x2, y, wall);
                }
            }

            if !cell.is_linked(cell.south()) {
                for x in x1..x2 {
                    image.put_pixel(x, y2, wall);
                }
            }
        }

        image
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "+{}", "---+".to_string().repeat(self.width()))?;
        for row in &self.cells {
            let mut top = "|".to_string();
            let mut bottom = "+".to_string();
            for cell in row {
                top.push_str("   ");
                top.push(if cell.is_linked(cell.east()) {
                    ' '
                } else {
                    '|'
                });
                bottom.push_str(if cell.is_linked(cell.south()) {
                    "   "
                } else {
                    "---"
                });
                bottom.push('+');
            }
            writeln!(f, "{}", top)?;
            writeln!(f, "{}", bottom)?;
        }
        Ok(())
    }
}

enum Cells<'a> {
    Empty,
    NonEmpty {
        columns: usize,
        i: usize,
        grid: &'a Grid,
    },
}

impl<'a> Iterator for Cells<'a> {
    type Item = Cell;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Cells::Empty => None,
            Cells::NonEmpty { columns, i, grid } => {
                let cell = grid.cell(*i / *columns, *i % *columns);
                *i += 1;
                cell
            }
        }
    }
}
